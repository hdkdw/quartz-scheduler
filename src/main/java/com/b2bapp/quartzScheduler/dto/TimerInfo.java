package com.b2bapp.quartzScheduler.dto;

public class TimerInfo {
    private int totalFireCount;
    private boolean runForever;
    private long repeatIntervalMs;
    private long initialOffsetMs;
    private  String callbackdata;


    public TimerInfo() {
        this.totalFireCount = totalFireCount;
        this.runForever = runForever;
        this.repeatIntervalMs = repeatIntervalMs;
        this.initialOffsetMs = initialOffsetMs;
        this.callbackdata = callbackdata;
    }

    public int getTotalFireCount() {
        return totalFireCount;
    }

    public void setTotalFireCount(int totalFireCount) {
        this.totalFireCount = totalFireCount;
    }

    public boolean isRunForever() {
        return runForever;
    }

    public void setRunForever(boolean runForever) {
        this.runForever = runForever;
    }

    public long getRepeatIntervalMs() {
        return repeatIntervalMs;
    }

    public void setRepeatIntervalMs(long repeatIntervalMs) {
        this.repeatIntervalMs = repeatIntervalMs;
    }

    public long getInitialOffsetMs() {
        return initialOffsetMs;
    }

    public void setInitialOffsetMs(long initialOffsetMs) {
        this.initialOffsetMs = initialOffsetMs;
    }

    public String getCallbackdata() {
        return callbackdata;
    }

    public void setCallbackdata(String callbackdata) {
        this.callbackdata = callbackdata;
    }
}
