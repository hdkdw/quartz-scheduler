package com.b2bapp.quartzScheduler.job;



import com.b2bapp.quartzScheduler.utility.DB_Connection;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.DbUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class CDRCreationJob implements Job {

    private static final Logger LOG = LoggerFactory.getLogger(CDRCreationJob.class);


    @Override
    public void execute(JobExecutionContext context) {
//        this below code will define job to be executed by scheduler

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            LocalDateTime dateTime = LocalDateTime.now();
            DateTimeFormatter timeStamp = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

            String filePath = "./src/main/resources/static/" + "cdr"+dateTime.format(timeStamp) + ".csv";
            PrintWriter pw = new PrintWriter(new File(filePath));
            StringBuilder sb = new StringBuilder();

            DB_Connection obj_DB_Connection = new DB_Connection();
            conn = obj_DB_Connection.getConnection();

            String query = "SELECT b.executionTime, pilotNumber, transactionId FROM mepro_preprod.smsrequest a INNER JOIN mepro_preprod.smstransaction b ON a.id = b.requestId WHERE a.status = 'success' AND b.executionTime >= (CURDATE() - INTERVAL 90 DAY) ORDER BY a.id;";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                sb.append(rs.getString("executionTime"));
                sb.append(";");
                sb.append(rs.getString("pilotNumber"));
                sb.append(";");
                sb.append(rs.getString("transactionId"));
                sb.append(";");
                sb.append("4008");
                sb.append("\r\n");

            }

            pw.write("execution_time;pilot_number;transaction_id;nums\r\n");
            pw.write(sb.toString());
            pw.close();

            LOG.info("files cdr created");
        } catch (Exception ex) {
            LOG.info(String.valueOf(ex));
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps);
            DbUtils.closeQuietly(conn);
        }


    }


}






