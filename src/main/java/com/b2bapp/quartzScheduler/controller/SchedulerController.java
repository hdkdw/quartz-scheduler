package com.b2bapp.quartzScheduler.controller;

import com.b2bapp.quartzScheduler.dto.TimerInfo;
import com.b2bapp.quartzScheduler.job.CDRCreationJob;
import com.b2bapp.quartzScheduler.scheduler.SchedulerService;
import org.quartz.SchedulerException;

public class SchedulerController{

    public void runHelloWorldJob() throws SchedulerException {

        TimerInfo info = new TimerInfo();
        info.setTotalFireCount(5);
        info.setRepeatIntervalMs(300000);
        info.setCallbackdata("here is the callback data ");

        SchedulerService schedulerService = new SchedulerService();
        schedulerService.schedule(CDRCreationJob.class, info);

    }

}
