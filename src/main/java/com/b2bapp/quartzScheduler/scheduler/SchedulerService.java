package com.b2bapp.quartzScheduler.scheduler;

import com.b2bapp.quartzScheduler.dto.TimerInfo;
import com.b2bapp.quartzScheduler.utility.TimerUtility;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SchedulerService {

    private static final Logger LOG = LoggerFactory.getLogger(SchedulerService.class);

    public void schedule(final Class jobClass, final TimerInfo info) throws SchedulerException {

        final JobDetail jobDetail = TimerUtility.buildJobDetail(jobClass, info);
        final Trigger trigger = TimerUtility.buildTrigger(jobClass, info);

        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        try{
            scheduler.start();
            scheduler.scheduleJob(jobDetail, trigger);

        } catch (SchedulerException e){
            LOG.error(e.getMessage(), e);
        }

    }

}
