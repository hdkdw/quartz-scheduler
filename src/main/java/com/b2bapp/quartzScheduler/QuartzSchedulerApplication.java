package com.b2bapp.quartzScheduler;

import com.b2bapp.quartzScheduler.controller.SchedulerController;
import org.quartz.SchedulerException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuartzSchedulerApplication {

	public static void main(String[] args) throws SchedulerException {
		SpringApplication.run(QuartzSchedulerApplication.class, args);
		var controllerJob = new SchedulerController();
		controllerJob.runHelloWorldJob();
	}

}
